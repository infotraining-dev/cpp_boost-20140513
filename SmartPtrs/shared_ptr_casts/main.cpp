#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace std;

class Base
{
public:
    virtual void do_stuff()
    {
        cout << "Base::do_stuff()" << endl;
    }

    ~Base()
    {
        cout << "~Base()" << endl;
    }
};

class Derived : public Base
{
public:
    virtual void do_stuff()
    {
        cout << "Derived::do_stuff()" << endl;
    }

    void derived_only()
    {
        cout << "Derived::derived_only()\n";
    }

    ~Derived()
    {
        cout << "~Derived()" << endl;
    }
};


int main()
{
    boost::shared_ptr<Base> ptr_base(new Derived());

    ptr_base->do_stuff();

    boost::shared_ptr<Derived> ptr_derived
            = boost::static_pointer_cast<Derived>(ptr_base);
    ptr_derived->derived_only();
}
