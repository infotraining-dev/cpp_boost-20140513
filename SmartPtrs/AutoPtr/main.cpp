#include <iostream>
#include <memory>
#include <cassert>
#include <vector>

using namespace std;

class X
{
    int id_;
public:
    X(int id = 0) : id_(id)
    {
        cout << "X(" << id_ << ")" << endl;
    }

    ~X()
    {
        cout << "~X(" << id_ << ")" << endl;
    }

    int id() const
    {
        return id_;
    }
};

auto_ptr<X> create_x(int id)
{
    return auto_ptr<X>(new X(id));
}

unique_ptr<X> create_unique_x(int id)
{
    return unique_ptr<X>(new X(id));
}

void print(auto_ptr<X> ptrx)
{
    cout << ptrx->id() << endl;
}

void print(unique_ptr<X> ptrx)
{
     cout << ptrx->id() << endl;
}

void print(X& x)
{
    cout << x.id() << endl;
}

int main()
{
    {
        auto_ptr<X> ptrx1 = create_x(1);
        cout << "id: " << ptrx1->id() << endl;

        auto_ptr<X> ptrx2 = ptrx1;
        assert(ptrx1.get() == 0);
        cout << "id: " << (*ptrx2).id() << endl;


        ptrx2.reset(new X(7));
        //print(ptrx2);

        cout << "id: " << (*ptrx2).id() << endl;

        X* raw_ptr = ptrx2.release();

        delete raw_ptr;
    }

    cout << "\nunique_ptrs:\n";

    {
        unique_ptr<X> named_ptr(new X(122));

        print(create_unique_x(122));

        unique_ptr<X> ptrx1(new X(1));
        cout << "id: " << ptrx1->id() << endl;

        unique_ptr<X> ptrx2 = move(ptrx1);
        cout << "id: " << ptrx2->id() << endl;

        unique_ptr<X> ptrx3 = create_unique_x(9);
        cout << "id: " << ptrx3->id() << endl;

        //print(move(ptrx3));

        print(*ptrx3);

        cout << "po print" << endl;

        vector<unique_ptr<X>> vecx;

        vecx.push_back(unique_ptr<X>(new X(99)));
        vecx.push_back(move(ptrx3));

        X& ref = *(vecx[0]);

        cout << "id: " << ref.id() << endl;

        unique_ptr<X[]> tab1(new X[10]);

        cout << "tab1[0].id() = " << tab1[0].id() << endl;

        named_ptr = move(vec[0]);
    }
}
