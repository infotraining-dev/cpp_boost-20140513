#include "server.hpp"
#include <deque>

using namespace std;

struct Server::Impl
{
    std::deque<char> buffer_;
};

Server::Server() : impl_(new Impl())
{
}

Server::Server(Server&& src) = default;

Server& Server::operator=(Server&& src) = default;

Server::~Server() = default;

void Server::send(const string &message)
{
    impl_->buffer_.resize(message.size() + 1);
    copy(message.begin(), message.end(), impl_->buffer_.begin());
    impl_->buffer_.back() = '\0';

    //...
}
