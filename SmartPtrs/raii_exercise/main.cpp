#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <memory>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}

// RAII handler for FILE
class FileGuard : boost::noncopyable
{
public:
    FileGuard(FILE* file) : file_(file)
    {
        if (file_ == 0)
            throw std::runtime_error("Blad otwarcia pliku!!!");
    }

    ~FileGuard()
    {
        fclose(file_);
    }

    FILE* get() const
    {
        return file_;
    }
private:
    FILE* file_;
};

// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    FileGuard file(fopen(file_name, "w"));

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_with_shared_ptr(const char* file_name)
{
    boost::shared_ptr<FILE> file(fopen(file_name, "w"),
                                 [](FILE* handle) { if(handle) fclose(handle); });

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

class FileCloser
{
public:
    void operator()(FILE* file)
    {
        fclose(file);
    }
};

void save_with_unique_ptr(const char* file_name )
{
    std::unique_ptr<FILE, FileCloser> file(fopen(file_name, "w"));

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    //save_with_shared_ptr("text.txt");
    save_with_unique_ptr("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

    std::string temp;
    std::getline(std::cin, temp);
}
