#include <iostream>
#include <queue>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/signals2.hpp>

using namespace std;

class Worker
{
    typedef boost::function<void ()> CommandType;
    queue<CommandType> tasks_;

    boost::signals2::signal<void()> on_tasks_completed_;
public:
    void register_task(CommandType cmd)
    {
        tasks_.push(cmd);
    }

    boost::signals2::connection register_complete_handler(CommandType cmd)
    {
        return on_tasks_completed_.connect(cmd);
    }

    void run()
    {
        while(!tasks_.empty())
        {
            CommandType cmd = tasks_.front();
            cmd();
            tasks_.pop();
        }

        on_tasks_completed_();
    }
};

class Printer
{
public:
    void print(const string& text)
    {
        cout << "Print:" << text << endl;
    }

    void off()
    {
        cout << "Printer.off\n";
    }

    void on()
    {
        cout << "Printer.on\n";
    }
};

void my_handler(const string& msg)
{
    cout << "Handler: " << msg << endl;
}

int main()
{
    Printer prn;

    Worker worker;

    worker.register_task(boost::bind(&Printer::on, &prn));
    worker.register_task(boost::bind(&Printer::print, &prn, "Tekst"));
    worker.register_task(boost::bind(&Printer::off, &prn));

    //...

    worker.register_complete_handler(boost::bind(&my_handler, "Bind: Tasks completed"));
    auto conn = worker.register_complete_handler([] { cout << "Lambda: Tasks completed."; });

    conn.disconnect();

    worker.run();

    return 0;
}

