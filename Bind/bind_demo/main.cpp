#include <iostream>
#include <boost/bind.hpp>
#include <boost/checked_delete.hpp>
#include <functional>
#include <boost/algorithm/string.hpp>

using namespace std;

void func1(int x, const string& s)
{
    cout << "func1(" << x << ", " << s << ")\n";
}

void func2(int x, double y, const string& s = "default")
{
    cout << "func2(" << x << ", " << y << ", " << s << ")\n";
}

class Functor //: public binary_function<int, int, void>
{
public:
    //typedef void result_type;

    void operator()(int x, int y)
    {
        cout << "x: " << x << " y: " << y << endl;
    }
};

void change_params(int& x, string& str)
{
    ++x;
    boost::to_upper(str);
}

class Foo
{
    string y_;
public:
    Foo(int x, const string& y) : x(x), y_(y) {}
    void do_sth(int a, const string& b)
    {
        cout << "Foo - " << x << " " << y_ << " " << a << " " << b << endl;
    }

    int x;

//    int x() const
//    {
//        return x_;
//    }
};

int main()
{
    boost::function<void (int)> f0 = boost::bind(&func1, 4, "four");
    f0(5);

    auto f1 = boost::bind(&func1, _1, "four");
    f1(7);

    auto f2 = boost::bind(&func2, _2, 3.14, _1);
    f2("one", 1);

    auto f4 = boost::bind<void>(Functor(), 9, _1);
    f4(9);

    int x = 10;
    string str = "hello";

    auto f5 = boost::bind(&change_params, boost::ref(x), boost::ref(str));
    f5();

    cout << x << " " << str << endl;

    Foo foo1(7, "seven");

    auto f6 = boost::bind(&Foo::do_sth, &foo1, 8, _1);

    f6("six");

    auto f7 = boost::bind(&Foo::do_sth, _1, 9, _2);

    f7(foo1, "six");

    cout << "\n";

    vector<Foo> vecfoo1 = { Foo{1, "one"}, Foo{2, "two"}, Foo{3, "three"} };

    for_each(vecfoo1.begin(), vecfoo1.end(),
             boost::bind(&Foo::do_sth, _1, 99, "nine"));

    auto where_gt_1 = find_if(vecfoo1.begin(), vecfoo1.end(),
                         boost::bind(greater<int>(),
                                     boost::bind(&Foo::x, _1), 1));

    int threshold = 1;

    auto where = find_if(vecfoo1.begin(), vecfoo1.end(),
                         //boost::bind(&Foo::x, _1) > 1);
                         [=](const Foo& foo) { return foo.x > threshold; });

    cout << "\n";

    vector<Foo*> vecfoo2 = { new Foo{1, "one"}, new Foo{2, "two"}, new Foo{3, "three"} };

    for_each(vecfoo2.begin(), vecfoo2.end(),
             boost::bind(&Foo::do_sth, _1, 99, "nine"));

    for_each(vecfoo2.begin(), vecfoo2.end(), &boost::checked_delete<Foo>);

    cout << "\n";

    vector<int> numbers = { 5, 3, 6, 22, 44, 67, 33, 2 };

    auto where1 = find_if(numbers.begin(), numbers.end(),
                         boost::bind(greater<int>(), _1, 50));

    auto where2 = find_if(numbers.begin(), numbers.end(),
                          [](int x) { return x > 50; });


}
