#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
#include <boost/algorithm/cxx11/copy_if.hpp>
#include <boost/bind.hpp>
#include <boost/range/algorithm.hpp>
#include <numeric>

using namespace std;

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "\nPracownicy z pensja powyżej 3000:\n";
    copy_if(employees.begin(), employees.end(),
                              ostream_iterator<Person>(cout, "\n"),
                              boost::bind(&Person::salary, _1) > 3000.0);


    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "\nPracownicy o wieku poniżej 30 lat:\n";


    // posortuj malejąco pracownikow wg nazwiska
    cout << "\nLista pracowników wg nazwiska (malejaco):\n";
    boost::range::sort(employees,
                       boost::bind(&Person::name, _1) > boost::bind(&Person::name, _2));
         //[](const Person& p1, const Person& p2) { return p1.name() > p2.name(); });

    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));

    // wyświetl kobiety
    cout << "\nKobiety:\n";

    // ilość osob zarabiajacych powyżej średniej
    cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";

    double sum = 0.0;
    for_each(employees.begin(), employees.end(),
             [&](const Person& p) { sum += p.salary(); });

    sum = accumulate(employees.begin(), employees.end(), 0.0,
                     //[](double x, const Person& p) { return x + p.salary();});
                     bind(plus<double>(), _1, bind(&Person::salary, _2)));

    cout << "Avg: " << sum / employees.size() << endl;
}
