#include <iostream>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

class Printer
{
public:
    void print(const string& msg, double arg)
    {
        cout << msg << arg << endl;
    }
};

class Logger
{
public:
    void log(const string& msg)
    {
        cout << "Log: " << msg << endl;
    }
};

void save_to_db(double temp)
{
    cout << "Saving to db: " << temp << endl;
}

class TemperatureMonitor
{
public:
    // zdefiniować typ sygnału TemperatureChangedSignal
    typedef boost::signals2::signal<void (double)> TemperatureChangedSignal;

    // zdefiniować typ slotów dla sygnału
    typedef TemperatureChangedSignal::slot_type TemperatureChangedSlot;

    TemperatureMonitor() : temp_(0.0) {}

    // implementacja podłączenia slotu do sygnału
    boost::signals2::connection register_handler(const TemperatureChangedSlot& slot)
    {
        return temp_changed_.connect(slot);
    }

    void set_temp(double temp)
    {
        if (temp_ != temp)
        {
            temp_ = temp;
            temp_changed_(temp_);
        }
    }

private:
    // sygnał zmiany temperatury
    TemperatureChangedSignal temp_changed_;
    double temp_;
};


int main()
{
    using namespace boost::signals2;

    Printer printer;
    TemperatureMonitor monitor;
    Logger logger;

    // podłączenie Printera
    monitor.register_handler(
                boost::bind(&Printer::print, &printer, "New temp: ", _1));

    // podłączenie Loggera
    monitor.register_handler(
                boost::bind(&Logger::log, &logger,
                            boost::bind(&boost::lexical_cast<string, double>, _1)));

    monitor.set_temp(24.0);
    monitor.set_temp(45.0);

    {
        // podłączenie save_to_db - ograniczone do zakresu
        // TODO
        boost::signals2::scoped_connection conn_local(monitor.register_handler(&save_to_db));

        monitor.set_temp(124.0);
        monitor.set_temp(245.0);
    }

    monitor.set_temp(45.0);

    {
        boost::shared_ptr<Printer> shared_prn(new Printer);

        monitor.register_handler(
                    TemperatureMonitor::TemperatureChangedSlot(
                        &Printer::print, shared_prn.get(), "Shared Printer: ",
                        _1).track(shared_prn));
        // podłączenie z shared_ptr<Printer>

        monitor.set_temp(400.0);
    }

    monitor.set_temp(45.0);
}
