#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>

using namespace std;

class AbsVisitor : public boost::static_visitor<float>
{
public:
    template <typename T>
    float operator()(const T& x) const
    {
        return abs(x);
    }

    float operator()(float f) const
    {
        return fabs(f);
    }

//    float operator()(const complex<double>& c) const
//    {
//        return abs(c);
//    }
};

int main()
{
    typedef boost::variant<int, float, complex<double> > VariantNumber;
    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(-7);
    vars.push_back(complex<double>(-1, -1));

    // TODO: korzystając z mechanizmu wizytacji wypisać na ekranie
    // moduły liczb

    AbsVisitor abs_visitor;
    transform(vars.begin(), vars.end(), ostream_iterator<float>(cout, " "),
              boost::apply_visitor(abs_visitor));
    cout << endl;
}
