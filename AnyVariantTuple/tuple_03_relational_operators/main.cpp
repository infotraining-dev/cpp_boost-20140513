#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/tuple/tuple_io.hpp>
#include "PersonalInfo.hpp"

using namespace std;

template <typename Tuple, int Index>
struct PrintHelper
{
    static void print(const Tuple& t)
    {
        PrintHelper<Tuple, Index-1>::print(t);
        cout << t.get<Index>() << "\n";
    }
};

template <typename Tuple>
struct PrintHelper<Tuple, 0>
{
    static void print(const Tuple& t)
    {
        cout << t.get<0>() << "\n";
    }
};

template <typename Tuple>
void meta_print(const Tuple& t)
{
    PrintHelper<Tuple, boost::tuples::length<Tuple>::value-1>::print(t);
}

int main()
{
    typedef boost::tuple<string, int, PersonalInfo> MyTuple;
    MyTuple t1("same?", 2, PersonalInfo("Nikodem", "Dyzma", 28));

    meta_print(t1);

    cout << "\n\n";
    cout << boost::tuples::length<MyTuple>::value << endl;

	boost::tuple<string, int, PersonalInfo> t2("same?", 2, PersonalInfo("Nikodem", "Dyzma", 28));
	boost::tuple<string, int, PersonalInfo> t3("different", 2, PersonalInfo("Nikodem", "Dyzma", 28));

	cout.setf(ios::boolalpha);
	cout << t1 << " == " << t2 << ": "  << (t1 == t2) << endl;
	cout << t1 << " == " << t3 << ": "  << (t1 == t3) << endl;

	typedef boost::tuple<string, int, PersonalInfo> tuple_3;
	vector<tuple_3> vec_of_tuples;

	vec_of_tuples.push_back(boost::make_tuple("1_one", 1, PersonalInfo("Nikodem", "Dyzma", 28)));
	vec_of_tuples.push_back(boost::make_tuple("5_five", 1, PersonalInfo("Nikodem", "Anonim", 22)));
	vec_of_tuples.push_back(boost::make_tuple("1_one", 1, PersonalInfo("Nikodem", "Anonim", 28)));
	vec_of_tuples.push_back(boost::make_tuple("1_one", 2, PersonalInfo("Nikodem", "Dyzma", 28)));
	vec_of_tuples.push_back(boost::make_tuple("4_four", 1, PersonalInfo("Nikodem", "Dyzma", 28)));

	sort(vec_of_tuples.begin(), vec_of_tuples.end());

	cout << "Posortowane krotki:\n";
	copy(vec_of_tuples.begin(),
			vec_of_tuples.end(),
			ostream_iterator<tuple_3>(cout, "\n"));
	cout << "\n" << endl;
}
