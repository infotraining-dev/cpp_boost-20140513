#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <boost/any.hpp>
#include <boost/assign.hpp>
#include <boost/bind.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/range/adaptors.hpp>

using namespace std;

template <typename Type>
class IsType
{
public:
    bool operator()(const boost::any& a) const
    {
        return a.type() == typeid(Type);
    }
};

int main()
{
    using namespace boost::assign;

    vector<boost::any> store_anything;
    store_anything += 1, 5, string("three"), 3, string("four"), string("one"),
                      string("eight"), 5, 4, boost::any(),
                      string("five"), string("six"), boost::any();

    /* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji stored_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
     * 3 - wyekstraktuj z kontenera store_anything do innego kontenera
     *     wszystkie elementy typu string
	 */

	// 1
	vector<boost::any> non_empty;
//    remove_copy_if(store_anything.begin(), store_anything.end(),
//                   back_inserter(non_empty), boost::mem_fn(&boost::any::empty));

    boost::range::push_back(non_empty,
                            store_anything | boost::adaptors::filtered(
                                             !boost::bind(&boost::any::empty, _1)));


	cout << "store_anything.size() = " << store_anything.size() << endl;
	cout << "non_empty.size() = " << non_empty.size() << endl;

	// 2
    // int count_int = count_if(store_anything.begin(), store_anything.end(), IsType<int>());
    int count_int = boost::range::count_if(store_anything, IsType<int>());
    cout << "stored_anything przechowuje " << count_int << " elementow typu int" << endl;

    int count_string = count_if(store_anything.begin(), store_anything.end(), IsType<string>());
    cout << "stored_anything przechowuje " << count_string << " elementow typu string" << endl;

	// 3
	list<string> string_items;

    string (*convert)(const boost::any&) = &boost::any_cast<string>;

    boost::range::transform(store_anything | boost::adaptors::filtered(IsType<string>()) | boost::adaptors::reversed,
                            back_inserter(string_items), convert);

	cout << "string_items: ";
	copy(string_items.begin(), string_items.end(),
			ostream_iterator<string>(cout, " "));
	cout << endl;

    // 4
    /*
    MapAny m;

    m.insert("id", 1);
    m.insert("name", string("Adam"));
    m.insert("age", 33);

    try
    {
        string id = m.get<string>("id");
        cout << "id = " << id << endl;
        string name = m.get<string>("name");
        cout << "name = " << name << endl;
        string address = m.get<string>("address");
    }
    catch(InvalidValueType& e)
    {
        cout << e.what() << endl;
    }
    catch(KeyNotFound& e)
    {
        cout << e.what() << endl;
    }
    */
}
