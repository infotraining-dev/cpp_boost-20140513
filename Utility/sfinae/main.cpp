#include <iostream>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>

using namespace std;

void foo(int x)
{
    cout << "foo(int: " << x << ")" << endl;
}

template <typename T>
typename boost::disable_if<boost::is_integral<T>>::type foo(T value)
{
    cout << "foo<T>(" << value << ")" << endl;
}

struct X
{
    typedef void nested_type;
    int x;
};

ostream& operator<<(ostream& out, const X& x)
{
    out << x.x;
    return out;
}

int main()
{
    int x = 10;
    foo(x);

    short sx = 10;
    foo(sx);

    X y { 15 };
    foo(y);
}
