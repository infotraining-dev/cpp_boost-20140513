#include <iostream>
#include <memory>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/is_integral.hpp>
#include <boost/type_traits/is_pointer.hpp>
#include <boost/shared_ptr.hpp>
#include <thread>
#include <future>
#include <chrono>

class Base {};

class Derived : public Base {};

class Different {};

template <typename T>
class OnlyCompatibleWithIntegralTypes
{
    //BOOST_STATIC_ASSERT(boost::is_integral<T>::value);
    static_assert(boost::is_integral<T>::value, "T musi byc calkowite");
};

template <typename T, size_t i>
void accepts_arrays_with_size_between_1_and_100(T (&arr)[i])
{
	BOOST_STATIC_ASSERT(i>=1 && i<=100);
}

void expects_ints_to_be_4_bytes()
{
	BOOST_STATIC_ASSERT(sizeof(int)==4);
}

template <typename T>
void works_with_base_and_derived(T& t)
{
	BOOST_STATIC_ASSERT((boost::is_base_of<Base, T>::value));
}

template <typename T>
class IsSharedPtr : public boost::false_type
{
};

template <typename T>
class IsSharedPtr<boost::shared_ptr<T>> : public boost::true_type
{
};

template <typename T>
class IsSharedPtr<std::shared_ptr<T>> : public boost::true_type
{
};

template <typename T>
void only_for_good_pointers(T ptr)
{
    BOOST_STATIC_ASSERT(boost::is_pointer<T>::value || IsSharedPtr<T>::value);

    std::cout << *ptr << std::endl;
}

int f(int i)
{
    std::cout << "f" << i << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    return i * i;
}

int main()
{
    std::future<int> result1 = std::async(std::launch::async, [] { return f(1);});
    auto result2 = std::async(std::launch::async, [] { return f(2);});

    std::cout << result1.get() << std::endl;
    std::cout << result2.get() << std::endl;

    int* ptr1 = new int(13);
    only_for_good_pointers(ptr1); // ok

    boost::shared_ptr<int> ptr2(new int(14));
    only_for_good_pointers(ptr2);

    std::auto_ptr<int> ptr3(new int(15));
    only_for_good_pointers(ptr3); // error

    OnlyCompatibleWithIntegralTypes<int> test1;

	int arr[50];

	accepts_arrays_with_size_between_1_and_100(arr);

	Derived arg;
	works_with_base_and_derived(arg);
}
