#include "Deleter.hpp"
#include "ToBeDeleted.hpp"
#include <vector>
#include <boost/assign.hpp>

class SomeClass;

SomeClass* create()
{
	return (SomeClass*)0;
}

void simple_test()
{
//    SomeClass* p = create();

//    delete p; // w najlepszym razie warning
}

void real_test()
{
	ToBeDeleted* tbd = new ToBeDeleted();

	Deleter exterminator;
	exterminator.delete_it(tbd);
}

int main()
{
    using boost::assign;
    std::vector<int> numbers = list_of(1)(2)(3)(4)(5);
    numbers += 1, 2, 3, 4, 5, 6, 7, 8, 9, 10;

	real_test();
}
