#include "Deleter.hpp"
#include <boost/checked_delete.hpp>

void Deleter::delete_it(ToBeDeleted* p)
{
    delete p;
    boost::checked_delete(p);
}
